@echo off
title Converter Build Script

set CWD=%~dp0

if "%1" neq "" (
    if "%1" equ "-c" goto clean
    if "%1" equ "-d" goto restore
    if "%1" equ "-h" goto help
    if "%1" equ "-r" goto run
)

:build
dotnet build -c Release -r win10-x64 -f net461 -v d
echo Build done
goto end

:restore
dotnet restore
echo Restore done
goto end

:run
dotnet run
goto end

:clean
if exist bin rmdir /s /q bin
if exist obj rmdir /s /q obj
echo Cleaning done
goto end

:help
echo Converter Build Script 1.0
echo Usage: build.bat [option]
echo Supplying no option will cause the tool to build the app
echo.
echo Options
echo    -c - Runs the binary cleanup
echo    -d - Runs the dotnet restore command
echo    -h - Shows this help text
echo    -r - Runs a debug build of the app
goto end

:end
