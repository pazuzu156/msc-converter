using System.Diagnostics;
using System;
using System.Collections.Generic;
using System.IO;

namespace Converter
{
    public class Convert
    {
        private List<string> extensions;

        public Convert()
        {
            this.extensions = new List<string> {
                ".mp4",
                ".mp3",
                ".ogg",
                ".wma", // probably not
                ".wav" // also probably not
            };
        }

        public void ConvertAllFiles()
        {
            var files = Directory.GetFiles(Directory.GetCurrentDirectory());

            foreach (var file in files) {
                if (extensions.Contains(Path.GetExtension(file))) {
                    var filename = file.Replace(Path.GetDirectoryName(file) + "\\", "");
                    ToOgg(filename);
                }
            }
        }

        public void ToOgg(string file)
        {
            if (FilePath.ExistsInPath("ffmpeg.exe")) {
                if (File.Exists(file)) {
                    if (extensions.Contains(Path.GetExtension(file))) {
                        string newfile = file.Replace(Path.GetExtension(file), ".ogg");
                        Console.WriteLine($"Converting {file} to {newfile}");

                        if (!Directory.Exists("oggs")) {
                            Directory.CreateDirectory("oggs");
                        }

                        ProcessStartInfo psi = new ProcessStartInfo();
                        psi.FileName = FilePath.GetFullPath("ffmpeg.exe");
                        psi.UseShellExecute = false;
                        psi.Arguments = $"-i \"{file}\" -ar 22050 -af \"pan=mono|c0=.5*c0+.5*c1\" \"oggs/{newfile}\" -y -loglevel panic";

                        var process = Process.Start(psi);
                        process.WaitForExit();

                        Console.WriteLine($"Conversion of {file} completed");
                    } else {
                        Error($"{file} isn't a supported audio file, skipping");
                    }
                } else {
                    Error($"{file} does not exist");
                }
            } else {
                Error("ffmpeg was not found in the system PATH");
            }
        }

        private void Error(string message)
        {
            Console.WriteLine($"Error: {message}");
        }
    }
}
