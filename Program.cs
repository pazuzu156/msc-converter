﻿using System.IO;
using System;
using System.Reflection;

namespace Converter
{
    class Program
    {
        private string[] cmdArgs = Environment.GetCommandLineArgs();

        public Program()
        {
            var convert = new Convert();

            if (cmdArgs.Length > 0) {
                // foreach (var arg in cmdArgs) {
                //     if (arg == "--help" || arg == "-h") {
                //         ShowHelp();
                //         break;
                //     } else {
                //         if (arg != Assembly.GetExecutingAssembly().GetName().Name + ".exe") {
                //             convert.ToOgg(arg);
                //         }
                //     }
                // }
                for (int i = 0; i < cmdArgs.Length; i++) {
                    if (cmdArgs[i] == "--help" ||cmdArgs[i] == "-h") {
                        ShowHelp();
                        break;
                    }

                    if (cmdArgs.Length > 1) {
                        if (cmdArgs[i] != Assembly.GetExecutingAssembly().GetName().Name + ".exe") {
                            convert.ToOgg(cmdArgs[i]);
                        }
                    }

                    if (cmdArgs.Length == 1) {
                        convert.ConvertAllFiles();
                    }
                }
            } else {
                convert.ConvertAllFiles(); // We'll try if there aren't any arguments
            }
        }

        private void ShowHelp()
        {
            string helptext = $@"My Summer Car Custom Radio Audio Converter

This converter is used to convert audio files
to a workable format for My Summer Car, and to
make it more realistic by making the files sound
more closely to how they would in 1995 on the radio.
Format is in mono and runs at 22050Hz

To use, just run the executable in the desired folder
with audio files you want to convert, otherwise you can
manually specify which files to convert.

Example:
convert.exe <- Will convert all audio files in current directory
convert.exe file1.mp3 file2.mp4 <- converts these two files

Flags:
    --help|-h - Shows this help text

(c) 2019 Kaleb Klein
MIT License";

            Console.WriteLine(helptext);
        }

        static void Main(string[] args)
        {
            new Program();
        }
    }
}
