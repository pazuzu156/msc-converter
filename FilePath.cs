using System;
using System.IO;

namespace Converter
{
    public class FilePath
    {
        public static bool ExistsInPath(string filename)
        {
            return GetFullPath(filename) != null;
        }

        public static string GetFullPath(string filename)
        {
            if (File.Exists(filename)) {
                return Path.GetFullPath(filename);
            }

            var v = Environment.GetEnvironmentVariable("PATH");

            foreach (var path in v.Split(';')) {
                var fullpath = Path.Combine(path, filename);

                if (File.Exists(fullpath)) {
                    return fullpath;
                }
            }

            return null;
        }
    }
}
